# flow

![Version: 0.1.4](https://img.shields.io/badge/Version-0.1.4-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

A Helm chart for Kubernetes

## Source Code

* <https://gitlab.com/pleio/helm-charts/flow-charts>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity settings for pod scheduling. |
| appConfig.celery.brokerUrl | string | `""` | Celery broker URL for task queueing. |
| appConfig.db.host | string | `""` | Hostname of the database. |
| appConfig.db.name | string | `""` | Name of the database. |
| appConfig.db.password | string | `""` | Password for the database. |
| appConfig.db.user | string | `""` | Username for the database. |
| appConfig.debug | bool | `false` | Whether to enable debug mode. |
| appConfig.deleteCaseDisabled | bool | `false` | Whether to disable case deletion. |
| appConfig.email.from | string | `""` | Email address to send emails from. |
| appConfig.email.host | string | `""` | SMTP server for email sending. |
| appConfig.email.password | string | `""` | Password for the email server. |
| appConfig.email.port | int | `0` | Port for SMTP server. |
| appConfig.email.useTls | bool | `false` | Whether to use TLS for email communication. |
| appConfig.email.user | string | `""` | Username for the email server. |
| appConfig.oauth.clientId | string | `""` | OAuth client ID. |
| appConfig.oauth.clientSecret | string | `""` | OAuth client secret. |
| appConfig.oauth.url | string | `""` | OAuth provider URL. |
| appConfig.secretKey | string | `""` | Secret key for the application. |
| autoscaling.enabled | bool | `false` | Enables autoscaling for the application based on CPU usage. |
| autoscaling.maxReplicas | int | `100` | Maximum number of replicas for autoscaling. |
| autoscaling.minReplicas | int | `1` | Minimum number of replicas for autoscaling. |
| autoscaling.targetCPUUtilizationPercentage | int | `80` | Target CPU utilization percentage for autoscaling. |
| background.worker.resources.limits.cpu | string | `"300m"` | CPU limit for the background worker. |
| background.worker.resources.limits.memory | string | `"1024Mi"` | Memory limit for the background worker. |
| background.worker.resources.requests.cpu | string | `"100m"` | CPU request for the background worker. |
| background.worker.resources.requests.memory | string | `"256Mi"` | Memory request for the background worker. |
| existingSecret | bool | `false` | Specifies whether an existing Kubernetes Secret is used. |
| fullnameOverride | string | `""` | Optionally override the full name of the application. |
| image.pullPolicy | string | `"IfNotPresent"` | Pull policy for the image. IfNotPresent will pull the image only if it is not present on the node. |
| image.repository | string | `"registry.gitlab.com/pleio/flow"` | Image repository for the container. |
| image.tag | string | `"latest"` | Tag for the image, defaulting to 'latest'. |
| imagePullSecrets | list | `[]` | List of image pull secrets to use when pulling the image from a private registry. |
| ingress.annotations | object | `{}` | Annotations to add to the ingress, e.g. kubernetes.io/ingress.class or kubernetes.io/tls-acme. |
| ingress.className | string | `""` | Ingress class to use. |
| ingress.enabled | bool | `false` | Enables Ingress for the application. |
| ingress.hosts | list | `[{"host":"chart-example.local","paths":[{"path":"/","pathType":"ImplementationSpecific"}]}]` | List of hosts and paths to configure for ingress. |
| ingress.tls | list | `[]` | TLS configuration for the ingress. |
| livenessProbe.failureThreshold | int | `6` | Timeout duration for each probe. |
| livenessProbe.initialDelaySeconds | int | `40` | Configures the liveness probe to check container health. |
| livenessProbe.periodSeconds | int | `10` | Delay before initiating the probe. |
| livenessProbe.successThreshold | int | `1` | Number of consecutive failures before marking the container as unhealthy. |
| livenessProbe.timeoutSeconds | int | `1` | Frequency of probe checks. |
| nameOverride | string | `""` | Optionally override the full name of the chart. |
| nodeSelector | object | `{}` |  |
| podAnnotations | object | `{}` | Annotations to add to the pods. |
| podLabels | object | `{}` | Labels to add to the pods. |
| podSecurityContext | object | `{}` |  |
| readinessProbe.failureThreshold | int | `6` | Timeout duration for each probe. |
| readinessProbe.initialDelaySeconds | int | `20` | Configures the readiness probe to check if the container is ready to serve traffic. |
| readinessProbe.periodSeconds | int | `10` | Delay before initiating the probe. |
| readinessProbe.successThreshold | int | `1` | Number of consecutive failures before marking the container as not ready. |
| readinessProbe.timeoutSeconds | int | `1` | Frequency of probe checks. |
| replicaCount | int | `1` | Number of replicas to create for the application. |
| resources | object | `{}` |  |
| securityContext | object | `{}` |  |
| service.port | int | `8000` | Port on which the service is exposed. |
| service.type | string | `"ClusterIP"` | Type of the service (ClusterIP, NodePort, LoadBalancer). |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account. |
| serviceAccount.create | bool | `false` | Specifies whether a service account should be created. |
| serviceAccount.name | string | `""` | Name of the service account to use. If not set and create is true, a name is generated using the fullname template. |
| storage.existingDataStorage | string | `""` | Name of the existing data storage to use. |
| storage.existingMediaStorage | string | `""` | Name of the existing media storage to use. |
| storage.volume.className | string | `""` | Class name for the persistent volume. |
| storage.volume.storageSize | string | `""` | Size of the persistent volume. |
| tolerations | list | `[]` | Tolerations for scheduling pods on nodes with specific taints. |
